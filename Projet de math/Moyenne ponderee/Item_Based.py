#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 16:08:29 2019
@title : Collaborative filtering - Item-Based
@author : LY Thomas - SAINT-SAENS Hugo
@version : 1.0
"""

import csv
import numpy as np
import math as math

# Constantes caractérisant la taille du jeu de données proposé
NB_UTILISATEUR = 100
NB_ITEM = 1000
# Constante caractérisant le nombre d'utilisateurs que l'on prend en compte pour calculer la moyenne
NB_TOP = 25

# Retourne les valeurs du fichier .csv dans une matrice
def lecture_csv():
    donnees = np.zeros((NB_UTILISATEUR,NB_ITEM))
    utilisateurCourant = 0
    with open('./toy_incomplet.csv', 'r') as csvfile:
        filereader = csv.reader(csvfile, delimiter=' ')
        for row in filereader:
            for j in range(NB_ITEM):
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1
    return donnees;

# Permet d'écrire dans un nouveau fichier .csv la matrice avec les items predit
def ecriture_csv(matrice):
    with open('collaborative_filtering_item_based.csv','w') as csvfile:
        filewriter = csv.writer(csvfile)
        for u in range(len(matrice)):
            chaine = ''
            for i in range(len(matrice[u])):
                chaine += str(matrice[u][i])+' '
            filewriter.writerow([chaine])

# Retourne la moyenne d'un item
def moyenne(k,matrice):
    somme = 0.0
    Nbitems = 0
    for j in range(len(matrice[k])):
        if matrice[k][j] != -1:
            somme = matrice[k,j] + somme
            Nbitems += 1
    if Nbitems == 0:
        return 0
    else:
        return somme / Nbitems

# Retourne la covariance entre l'item x et y à l'aide de leurs moyennes
def cov(x,y,m1,m2):
    cov = 0
    for j in range(len(matrixItemCommun[y])):
        if (matrixItemCommun[x,j] != -1 and matrixItemCommun[y,j] != -1):
            cov = cov + ((matrixItemCommun[x,j] - m1) * (matrixItemCommun[y,j] - m2))
    return (cov / len(matrixItemCommun[y]))

# Retourne la variance d'un item à l'aide de sa moyenne
def var(k,m,matrice):
    v = 0
    for j in range(len(matrice[k])):
        if (matrice[k,j] != -1):
            v = v + (matrice[k,j] - m)**2
    return (v / len(matrice[k]))

# Retourne la similarité entre l'item x et y à l'aide de leur covariance commune et de leurs variances
def similarite(x,y,c,varx,vary):
    deno = math.sqrt(varx) * math.sqrt(vary)
    for j in range(len(matrixItemCommun[y])):
        if (matrixItemCommun[x,j] != -1 and matrixItemCommun[y,j] != -1):
            if deno != 0:
                return (c / deno)
            else:
                return 0

# Classe caractérisant un item à l'aide de sa place dans la matrice ainsi que sa similarité 
class Item:
    def __init__(self,numero):
        self.numero=numero
        self.similarite=0.0
    
    def setSimilarite(self,s):
        self.similarite=s
    
    def getNumero(self):
        return self.numero
    
    def getSimilarite(self):
        return self.similarite
    
# Retourne la moyenne des notes des NB premiers
def moyennePondereeUtilisateurs(topNBliste,itemCourant,matrice):
    somme = 0
    similariteTotal = 0.0
    for i in range(len(topNBliste)):
        if matrice[topNBliste[i].getNumero()][itemCourant] != -1:
            somme += matrice[topNBliste[i].getNumero()][itemCourant] * topNBliste[i].getSimilarite()
            similariteTotal += topNBliste[i].getSimilarite()
    if similariteTotal == 0.0:
        return 0
    else:
        return round(somme / similariteTotal)

# Nous créons la matrice correspondant au fichier .csv donné et nous la transposons 
# pour faire un filtrage collaboratif basé sur les items
donnees = lecture_csv().astype(np.int).T
# Nous dupliquons la matrice précédente pour obtenir une matrice immuable 
# afin d'analyser quels sont les items en commun d'un utilisateur et d'un autre
donneesDeBase = lecture_csv().astype(np.int).T

# Nous parcourons les items
for itemCourant in range(len(donneesDeBase)):
    # Nous calculons la moyenne et la variance de l'item courant
    moyenneItemCourant = moyenne(itemCourant,donneesDeBase)
    varx = var(itemCourant,moyenneItemCourant,donneesDeBase)
    # Nous parcourons les utilisateurs de l'item actuel
    for utilisateurCourant in range(len(donneesDeBase[itemCourant])):
        print("item n°",itemCourant," utilisateur n°",utilisateurCourant)
        # Si un utilisateur est trouvé sans note alors
        if donneesDeBase[itemCourant][utilisateurCourant] == -1:
            # Nous créons une nouvelle matrice transposée initialisée avec des zéros 
            # et qui a pour taille le jeu de données voulu
            matrixItemCommun=np.zeros(shape=(NB_UTILISATEUR,NB_ITEM)).astype(np.int).T
            # Nous y insèrons les utilisateurs de l'item actuel 
            matrixItemCommun[itemCourant]=donneesDeBase[itemCourant]
            # Nous créons une liste vide d'items
            listeItem = []
            # Nous parcourons de nouveau la matrice pour connaitre les utilisateurs en commun de l'item courant avec l'item comparé
            for itemCompare in range(len(donneesDeBase)):
                # Nous créons l'item compare à l'aide de sa place dans la matrice 
                item=Item(itemCompare)
                # Nous parcourons les utilisateurs de celui-ci
                for utilisateurCompare in range(len(donneesDeBase[itemCompare])):
                    # Si l'item courant est différent de l'item comparé alors 
                    if (itemCourant != itemCompare): 
                        # Si les deux items ont un utilisateur en commun alors
                        if (donneesDeBase[itemCompare][utilisateurCompare] != -1) and (donneesDeBase[itemCourant][utilisateurCompare] != -1):
                             # Nous insèrons dans la matrice la note de l'utilisateur compare de l'item comparé
                             matrixItemCommun[itemCompare][utilisateurCompare] = donneesDeBase[itemCompare][utilisateurCompare]
                        else:
                            # Sinon nous y insèrons un utilisateur non noté
                            matrixItemCommun[itemCompare][utilisateurCompare] = -1
                # Nous ajoutons l'item dans la liste d'items
                listeItem += [item]
                # Si l'item courant est différent de l'item comparé alors
                if (itemCourant != itemCompare):
                    # Nous calculons la moyenne de l'item comparé ainsi que sa similarité 
                    # que l'on ajoute à l'item en question qui se trouve dans la liste d'items
                    moyenneItemCompare = moyenne(itemCompare,matrixItemCommun)
                    listeItem[itemCompare].setSimilarite(similarite(itemCourant,itemCompare,cov(itemCourant,itemCompare,moyenneItemCourant,moyenneItemCompare),varx,var(itemCompare,moyenneItemCompare,matrixItemCommun)))
            # Nous créons une liste comportant les NB_TOP premiers utilisateurs ayant la plus grande similarité
            top=sorted(listeItem, key=lambda i: i.similarite, reverse=True)[:NB_TOP]
            # Nous vidons la liste d'items pour une utilisation ultérieur
            del listeItem[:]
            # Nous insérons la moyenne des items des NB_TOP premiers items dans la première matrice 
            donnees[itemCourant][utilisateurCourant] = moyennePondereeUtilisateurs(top,utilisateurCourant,donnees)
            # Nous vidons la liste des NB_TOP premiers items
            # ayant la plus grande similarité pour une utilisation ultérieur
            del top[:]
# Nous créons un nouveau fichier .csv avec les notes prédites et nous transposons la matrice
# pour placer les utilisateurs en ligne comme dans le fichier de départ
ecriture_csv(donnees.T)