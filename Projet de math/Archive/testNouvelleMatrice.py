#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 20 10:28:59 2019

@author: hugo
"""

import csv
import numpy as np

#Fonction permettant d'insérer les  valeurs du fichier .cvs dans une matrice
def lecture_csv():
    utilisateur = 100
    item = 1000
    fichier = './toy_incomplet.csv'
    donnees = np.zeros((utilisateur,item)) #initialise la matrice [Utilisateurs,Items] à zéros
    utilisateurCourant = 0
    with open(fichier, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ')
        for row in spamreader:
            for j in range(item): #rempli la matrice avec les données du fichier csv
                donnees[utilisateurCourant,j] = row[j]
            utilisateurCourant+=1 #permet de passer à l'utilisateur suivant
    return donnees;

donnees = lecture_csv().astype(np.int64) #Transtypage du tableau de float en int
#print(type(donnees[0][50])) #on regarde que le tableau ait bien été transtypé

donnees = lecture_csv()

for utilisateurCourant in range(len(donnees)):
    for itemCourant in range(len(donnees[utilisateurCourant])):
        if donnees[utilisateurCourant][itemCourant] == -1:
            # On creer une nouvelle matrice 
            matrixItemCommun=np.zeros(shape=(100,1000))
            if ((utilisateurCourant != utilisateurCompare) and (itemCourant != itemCompare)) and ((donnees[utilisateurCourant][itemCourant] != -1) and (donnees[utilisateurCompare][itemCompare] != -1)):
                matrixItemCommun[utilisateurCourant][itemCourant]=donnees[utilisateurCourant][itemCourant]
            else:
                matrixItemCommun[utilisateurCourant][itemCourant]=-1
            
print(matrixItemCommun)