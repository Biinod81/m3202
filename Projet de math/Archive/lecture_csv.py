#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 08:26:13 2019

@author: jerome.fehrenbach
"""

import csv
import numpy as np

n = 100
m = 1000


fichier = './toy_complet.csv'
donnees = np.zeros((n,m))
i = 0
with open(fichier, 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    for row in spamreader:
       print(', '.join(row))